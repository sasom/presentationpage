## Where to start?

Type `make` to see all available commands.

## Run development environment

If you are running project for a first time, type `make setup` first.

When everything is set up, type `make run` to start development environment.