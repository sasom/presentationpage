help:			# Shows this help
	@grep "#\s" $(MAKEFILE_LIST)

setup:			# Run setup
	npm install

run:			# Run application
	npm run start

build:			# Builds app
	npm run build

build/prod:		# Builds app for production
	npm run build:prod
		
test: 			# Run all tests
test: test/format test/lint test/unit

test/unit:		# Run unit tests
	npm run test:unit

test/unit/watch:	# Run unit tests in watch mode
	npm run test:unit:watch

test/lint:		# Runs lint
	npm run test:lint

test/format:		# Runs prettier check
	npm run test:prettier

format:			# Formats code
format: format/prettier format/lint

format/lint:		# Runs lint with auto fix
	npm run format:lint

format/prettier:	# Runs prettier
	npm run format:prettier
