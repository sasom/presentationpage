import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './views/home.component';
import { TitleSectionComponent } from './components/title-section/title-section.component';
import { ProductSectionComponent } from './components/product-section/product-section.component';
import { CurveComponent } from './components/curve/curve.component';
import { ContactSectionComponent } from './components/contact-section/contact-section.component';
import { FooterComponent } from '../layout/footer/footer.component';

@NgModule({
  declarations: [
    HomeComponent,
    TitleSectionComponent,
    ProductSectionComponent,
    CurveComponent,
    ContactSectionComponent,
  ],
  imports: [CommonModule, HomeRoutingModule],
})
export class HomeModule {}
