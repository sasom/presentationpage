import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductSectionComponent } from './product-section.component';

describe('ProductSectionComponent', () => {
  let component: ProductSectionComponent;
  let fixture: ComponentFixture<ProductSectionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductSectionComponent],
    });

    fixture = TestBed.createComponent(ProductSectionComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should match the snapshot', () => {
    component.icon = 'assets/img/test.png';
    component.subtitle = 'Test subtitle text';
    component.title = 'Test title';
    fixture.detectChanges();

    expect(fixture).toMatchSnapshot();
  });
});
