import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-product-section',
  templateUrl: './product-section.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductSectionComponent {
  @Input() icon: string;
  @Input() title: string;
  @Input() subtitle: string;
}
