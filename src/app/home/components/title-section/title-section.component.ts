import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-title-section',
  templateUrl: './title-section.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TitleSectionComponent {
  @Input() title: string;
  @Input() subtitle: string;
}
