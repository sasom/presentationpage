import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactSectionComponent } from './contact-section.component';

describe('ContactSectionComponent', () => {
  let component: ContactSectionComponent;
  let fixture: ComponentFixture<ContactSectionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ContactSectionComponent],
    });

    fixture = TestBed.createComponent(ContactSectionComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should match snapshot', () => {
    component.title = 'Get in touch';
    component.email = 'test@test.com';
    fixture.detectChanges();

    expect(fixture).toMatchSnapshot();
  });
});
