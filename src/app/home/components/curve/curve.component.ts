import {
  Component,
  AfterViewInit,
  ViewChild,
  HostListener,
  ElementRef,
  Input,
  ChangeDetectionStrategy,
  NgZone,
} from '@angular/core';

interface Point {
  x: number;
  y: number;
}

interface Size {
  width: number;
  height: number;
}

@Component({
  selector: 'app-curve',
  templateUrl: './curve.component.html',
  styleUrls: ['./curve.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CurveComponent implements AfterViewInit {
  @Input() nOfSections: number;
  @Input() colors: Array<string> = ['#009688', '#F44336', '#2196F3', '#FFEB3B'];
  @Input() seed: number = 10;

  @ViewChild('canvas', { static: false }) canvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;

  frame: number = 0;
  resize: boolean = false;

  constructor(private readonly ngZone: NgZone) {}

  ngAfterViewInit(): void {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    const screen: Size = this.calculateCanvas();

    if (window.requestAnimationFrame) {
      this.animate(screen);
    } else {
      this.drawCurve(100, screen);
    }
  }

  @HostListener('window:resize')
  onResize(): void {
    this.resize = true;
    const screen: Size = this.calculateCanvas();
    this.drawCurve(100, screen);
  }

  async animate(screen: Size): Promise<void> {
    if (this.frame === 100 || this.resize) {
      return;
    }

    this.frame++;
    this.drawCurve(this.frame, screen);
    this.ngZone.runOutsideAngular(() =>
      window.requestAnimationFrame(() => this.animate(screen))
    );
  }

  calculateCanvas(): Size {
    let screen: Size;

    if (innerWidth > innerHeight) {
      screen = this.landscapeSize();
    } else {
      screen = this.portraitSize();
    }

    this.setCanvasSize(screen.width, screen.height);

    return screen;
  }

  drawCurve(percent: number, screen: Size): void {
    this.ctx.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );

    const points: Array<Point> = this.calculateCurvePoints(
      screen.width,
      screen.height,
      12
    );
    this.addStartingAndEndingCircles(screen.width, screen.height, points);

    const index = Math.floor((points.length * percent) / 100);
    this.drawCurveWithCircles(
      points.slice(0, index),
      screen.width * 0.033,
      this.seed
    );
  }

  portraitSize(): Size {
    // This should match css .section
    return {
      width: innerWidth,
      height: innerWidth * 0.5625,
    };
  }

  landscapeSize(): Size {
    // This should match css .section
    return {
      width: innerWidth,
      height: innerHeight < innerWidth * 0.45 ? innerWidth * 0.45 : innerHeight,
    };
  }

  setCanvasSize(width: number, height: number): void {
    const dpi = window.devicePixelRatio || 1;

    const el = this.canvas.nativeElement;
    el.width = width * dpi;
    el.height = this.nOfSections * height * dpi;
    el.style.width = width + 'px';
    el.style.height = this.nOfSections * height + 'px';
    el.style.minHeight = this.nOfSections * height + 'px';

    this.ctx.scale(dpi, dpi);
  }

  calculateCurvePoints(
    width: number,
    height: number,
    nOfCircles: number
  ): Array<Point> {
    const points: Array<Point> = new Array<Point>();

    let p1: Point;
    let cp1: Point;
    let cp2: Point;
    let p2: Point;

    for (let i = 0; i < this.nOfSections - 1; i++) {
      if (i % 2 === 0) {
        p1 = { x: width * 0.95, y: height * (0.5 + i) };
        cp1 = { x: width * 0.95, y: height * (1.66 + i) };
        cp2 = { x: width * 0.05, y: height * (0.33 + i) };
        p2 = { x: width * 0.05, y: height * (1.5 + i) };
      } else {
        p1 = { x: width * 0.05, y: height * (0.5 + i) };
        cp1 = { x: width * 0.05, y: height * (1.66 + i) };
        cp2 = { x: width * 0.95, y: height * (0.33 + i) };
        p2 = { x: width * 0.95, y: height * (1.5 + i) };
      }

      for (let j = 0; j < nOfCircles; j++) {
        const point = this.getBezierXY(j / nOfCircles, p1, cp1, cp2, p2);
        points.push(point);
      }
    }

    // Remove first point
    points.splice(0, 1);

    return points;
  }

  addStartingAndEndingCircles(
    width: number,
    height: number,
    points: Array<Point>
  ): void {
    // First two circles
    points.unshift({ x: width * 0.92, y: height * 0.59 });
    points.unshift({ x: width * 0.87, y: height * 0.5 });

    // Last two circles
    if (this.nOfSections % 2 === 0) {
      points.push({ x: width * 0.08, y: height * (this.nOfSections - 0.58) });
      points.push({ x: width * 0.14, y: height * (this.nOfSections - 0.49) });
    } else {
      points.push({ x: width * 0.92, y: height * (this.nOfSections - 0.58) });
      points.push({ x: width * 0.86, y: height * (this.nOfSections - 0.5) });
    }
  }

  drawCurveWithCircles(
    points: Array<Point>,
    radius: number,
    seed: number
  ): void {
    let s: number = seed;
    const random = (min: number, max: number): number => {
      return min + Math.floor(Math.abs(Math.sin(s++)) * (max - min + 1));
    };

    for (let i = 0; i < points.length; i++) {
      let r: number;

      // First two and last two points are smaller
      if (i === 0 || i === points.length - 1) {
        r = 0.5 * radius;
      } else if (i === 1 || i === points.length - 2) {
        r = 0.6 * radius;
      } else {
        r = random(radius * 0.8, radius);
      }

      this.drawCircle(points[i], r, this.colors[i % 4]);
    }
  }

  drawCircle(p: Point, r: number, color: string): void {
    this.ctx.beginPath();
    this.ctx.arc(p.x, p.y, r, 0, 2 * Math.PI);
    this.ctx.fillStyle = color;
    this.ctx.fill();
    this.ctx.closePath();
  }

  getBezierXY(t: number, p1: Point, cp1: Point, cp2: Point, p2: Point): Point {
    return {
      x:
        (1 - t) * (1 - t) * (1 - t) * p1.x +
        3 * t * (1 - t) * (1 - t) * cp1.x +
        3 * t * t * (1 - t) * cp2.x +
        t * t * t * p2.x,
      y:
        (1 - t) * (1 - t) * (1 - t) * p1.y +
        3 * t * (1 - t) * (1 - t) * cp1.y +
        3 * t * t * (1 - t) * cp2.y +
        t * t * t * p2.y,
    };
  }
}
