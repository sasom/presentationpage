import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockComponent } from 'ng-mocks';

import { HomeComponent } from './home.component';
import { CurveComponent } from '../components/curve/curve.component';
import { TitleSectionComponent } from '../components/title-section/title-section.component';
import { ProductSectionComponent } from '../components/product-section/product-section.component';
import { ContactSectionComponent } from '../components/contact-section/contact-section.component';
import { FooterComponent } from '../../layout/footer/footer.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        MockComponent(CurveComponent),
        MockComponent(TitleSectionComponent),
        MockComponent(ProductSectionComponent),
        MockComponent(ContactSectionComponent),
      ],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should match snapshot', () => {
    fixture.detectChanges();

    expect(fixture).toMatchSnapshot();
  });
});
